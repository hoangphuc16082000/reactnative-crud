import React, {useRef} from 'react';
import {NavigationContainer, useNavigation, useRoute} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Landing from '../View/Landing';
import Registration from '../View/Registration';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faChevronLeft} from '@fortawesome/free-solid-svg-icons';
import {TouchableOpacity, Text} from 'react-native';
import Home from '../View/Home';
import { EmployeeDetails } from '../View/EmployeeDetails';

const Stack = createStackNavigator();

export const AuthStack = (props) => {
   
  // 
  const navigationRef = useRef(null);

  let currentRoute = navigationRef.current != null ? navigationRef.current.getCurrentRoute().name : "Home"
  console.log(currentRoute)
  return (
    <NavigationContainer ref={navigationRef}>
     
      <Stack.Navigator>
        <Stack.Screen
          name="Auth"
          component={Landing}
          options={{
            title: 'Api Training App',
          }}
        />
        <Stack.Screen
          name="Register"
          component={Registration}
          options={{
            title: 'Registration',
            headerLeft: () => {
              return (
                <TouchableOpacity
                  onPress={() => navigationRef.current.goBack()}
                  style={{marginLeft: 20}}>
                  <FontAwesomeIcon size={20} icon={faChevronLeft} />

                  {/* <Text>TEST</Text> */}
                </TouchableOpacity>
              );
            },
          }}
        />
        <Stack.Screen
          name="Home"
          component={HomeStacok}
          options={{
            title: 'Api Training App',
            headerShown: false,
            headerLeft: null
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export const HomeStacok = (props) => {
  const navigation = useNavigation();
  
   return (
     <Stack.Navigator>
       <Stack.Screen
         name="Home"
         component={Home}
         options={{
           headerShown: false,
         }}
       />
       <Stack.Screen
         name="EmployeeDetails"
         component={EmployeeDetails}
         options={{
           headerShown: false,
         }}
       />
     </Stack.Navigator>
   );
}
