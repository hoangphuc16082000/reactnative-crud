import axios from 'axios'

export const ApiCall = (obj) => {
    
    let finalUrl = 'https://api-training-sanctum.dtsgroup0.com/api/' + obj.url
    // let finalUrl = 'https://samplecrud-dev-as.azurewebsites.net/api/' + obj.url
    
    return axios({
        method: obj.method,
        headers: obj.headers != undefined ? obj.headers : null,
        url: finalUrl,
        data:  obj.body != null && obj.body,
        // params:  obj.body
    }).then(res => {
        console.log(res)
        // if php use res.data.data
        // if .net user res.data
        return {
            status: res.status,
            data: res.data.data

        }
    }).catch(err => {
        console.log(err.response)
        return{
            status: err.response.status
        }
    })
}