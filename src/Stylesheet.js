import React from 'react'
import { StyleSheet } from "react-native";

export const globalStyle = StyleSheet.create({
  button: {
    paddingHorizontal: 25,
    paddingVertical: 10,
    backgroundColor: '#933401',
  },
  buttonText: {
    color: '#fff',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});