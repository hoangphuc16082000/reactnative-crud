import { Button } from 'native-base';
import React, {useEffect, useState} from 'react'
import { View, Text, Alert, ActivityIndicator } from 'react-native'
import { ApiCall } from '../api/api';
import { FormInput } from "../Component/Form";
import { globalStyle } from "../Stylesheet";
const Registration = (props) => {

    const [initialState, setInitialState] = useState({
      name: '',
      email: '',
      password: '',
      confirm_password: '',
    });
    const [loadiing, setLoading] = useState(false)



    useEffect(() => {
       
    },[loadiing])

    
    let array = [
      {
        id: 1,
        name: 'name',
        placeholder: 'name',
      },
      {
        id: 2,
        name: 'email',
        placeholder: 'email',
      },
      {
        id: 3,
        name: 'password',
        placeholder: 'password',
      },
      {
        id: 4,
        name: 'confirm_password',
        placeholder: 'confirm your password',
      },
    ];

    const submitForm = () => {
        setLoading(true)
        let bodyObj = {
            method: "post",
            headers: null,
            body: initialState,
            url: "register"
        }
        
        ApiCall(bodyObj).then(res => {
            // Alert message
            if(res.status == 200){
                Alert.alert("Success", "Please sign in.", [{
                    onPress: () => {props.navigation.navigate('Auth')}
                }])
            }
            else{
                Alert.alert('Failed', 'Please try again.', [
                  {
                    onPress: () => {
                            // Nothing to do here
                    },
                  },
                ]);
            }
                  setLoading(false);
        })
  
    }
    
    return (
      <View style={[globalStyle.container]}>
        <FormInput
          array={array}
          onChangeText={(val) => setInitialState({...initialState, ...val})}
        />
        <View style={{marginTop: 20}}>
          {loadiing ? (
            <ActivityIndicator size="large"  />
          ) : (
            <Button onPress={submitForm} style={globalStyle.button}>
              <Text style={globalStyle.buttonText}>Register</Text>
            </Button>
          )}
        </View>
      </View>
    );
}

export default Registration
