import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect, useState} from 'react';
import {View, Alert, SafeAreaView, ActivityIndicator, Text} from 'react-native';
import {ApiCall} from '../api/api';
import EmployeeList from '../Component/EmployeeList';

const Home = (props) => {
  const [employeeList, setEmployeeList] = useState([]);
  const [loading, setLoading] = useState(true)
  const [userData, setUserData] = useState({})
  useEffect(() => {
    props.navigation.setOptions({
      title: 'Home',
      headerShown: true,
      headerLeft: null,
    });


    getList();

    return () => {};
  }, []);

  const getList = async () => {

    // employee/passport for passport
    //employee for sanctum
    
    AsyncStorage.getItem('@user_info').then((res) => {
          let data;
      data = res != null ? JSON.parse(res) : null;
      setUserData(data)
        
      let objBody = {
        method: 'get',
        headers: {
          Authorization: 'Bearer ' + data.token,
          'Content-type': 'application/json',
        },
        body: null,
        url: 'employee/passport',
      };
      ApiCall(objBody).then((res) => {
        
        setEmployeeList([...res.data]);
        setLoading(false)
      });
    });
  };

  const onDelete = (item) => {
    AsyncStorage.getItem('@user_info').then((res) => {
      data = res != null ? JSON.parse(res) : null;
      let objBody = {
        method: 'delete',
        headers: {
          Authorization: 'Bearer ' + data.token,
          'Content-type': 'application/json',
        },
        body: null,
        url: 'employee/' + item.id,
      };
      ApiCall(objBody).then((res) => {
        if (res.status == 200) {
          Alert.alert('Success', 'Successfully deleted employee data.', [
            {
              onPress: () => {
                getList();
              },
            },
          ]);
        } else {
          Alert.alert(
            'Failed',
            'Cannot delete employee data, please try again.',
            [{}],
          );
        }
      });
    });
  };
  

  return (
    <SafeAreaView style={{flex: 1}}>

      {/* <Text>Welcome <Text>{userData !</Text></Text> */}


      {loading ? (
        <ActivityIndicator style={{alignSelf:"center"}} size="large" />
      ) : (
        <EmployeeList delete={onDelete} data={employeeList} />
      )}
    </SafeAreaView>
  );
};

export default Home;
