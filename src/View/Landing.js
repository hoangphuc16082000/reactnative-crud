import AsyncStorage from '@react-native-async-storage/async-storage';
import {Button} from 'native-base';
import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Alert, ActivityIndicator} from 'react-native';
import {ApiCall} from '../api/api';
import {FormInput} from '../Component/Form';
import {globalStyle} from '../Stylesheet';

const Landing = (props) => {
  const [initialState, setInitialState] = useState({
    email: '',
    password: '',
  });
  const [loading, setLoading] = useState(false);

  useEffect(() => {}, [loading]);

  let array = [
    {id: 1, name: 'email', placeholder: 'Enter your email address.'},
    {id: 2, name: 'password', placeholder: 'Enter your password.'},
  ];

  const submitForm = () => {
    // Login/auth for passport
    //login for sanctum

    setLoading(true);
    let bodyObj = {
      method: 'post',
      headers: null,
      body: initialState,
      url: 'login',
    };

    ApiCall(bodyObj).then((res) => {
      // Alert message
      console.log('LOGIN RES: ', res);
      if (res.status == 200) {
        Alert.alert('Welcome', 'Api Trainee', [
          {
            onPress: () => {
              // props.navigation.navigate('Auth');
              storeData(res.data);
         
            },
          },
        ]);
      } else {
        Alert.alert('Failed', 'Please try again.', [
          {
            onPress: () => {
              // Nothing to do here
            },
          },
        ]);
      }
      setLoading(false);
    });
  };

  const storeData = async (value) => {
    try {
      const jsonValue = JSON.stringify(value);
      await AsyncStorage.setItem('@user_info', jsonValue);
           props.navigation.navigate('Home');
    } catch (e) {
      console.log(e)
      // saving error
    }
  };

  return (
    <View style={globalStyle.container}>
      <FormInput
        email={initialState.email}
        password={initialState.password}
        onChangeText={(val) => setInitialState({...initialState, ...val})}
        array={array}
      />
      {loading ? (
        <ActivityIndicator style={{marginTop: 20}} size="large" />
      ) : (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            width: '80%',
            marginTop: 20,
          }}>
          <Button onPress={submitForm} style={globalStyle.button}>
            <Text style={globalStyle.buttonText}>Login</Text>
          </Button>
          <Button
            onPress={() => props.navigation.navigate('Register')}
            style={globalStyle.button}>
            <Text style={globalStyle.buttonText}>Register</Text>
          </Button>
        </View>
      )}
    </View>
  );
};

const style = StyleSheet.create({});
export default Landing;
