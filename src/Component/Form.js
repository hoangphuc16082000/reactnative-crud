import React, {useState, useEffect} from 'react';
import {View, Text, TextInput, StyleSheet, Dimensions} from 'react-native';
import { globalStyle } from '../Stylesheet';

const {width} = Dimensions.get('screen');
export const FormInput = (props) => {
  const [initialValues, setInitialValues] = useState({});
  useEffect(() => {}, [initialValues]);

  let array = props.array == undefined ? [] : props.array;
    
//    setInitialValues({...initialValues, ...{[item.name]: val}});

  return (
    <>
      <View>
        {array.length != 0 &&
          array.map((item, index) => {
            return (
              <View key={index} style={{marginTop: 10}}>
                <TextInput
                  secureTextEntry={item.name == "password" || item.name == "confirm_password" ? true : false}
                  autoCapitalize={"none"}
                  key={index}
                  value={props[item.name]}
                  editable={props.editable}
                  onChangeText={(val) => {
                       setInitialValues({...initialValues, ...{[item.name]: val}});
                       props.onChangeText({...initialValues, ...{[item.name]: val}})
                  }}
                  
                  placeholder={item.placeholder}
                  style={[style.container, globalStyle.shadow]}
                />
              </View>
            );
          })}
      </View>
    </>
  );
};

const style = StyleSheet.create({
  container: {
    width: width * 0.8,
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingVertical: 15,
    paddingHorizontal: 10,
  },
 
});
