import { faEdit, faTrash, faUser } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import React from 'react'
import { useState } from 'react';
import { View, Text, FlatList, TouchableOpacity, StyleSheet, Dimensions, Animated, PanResponder, TouchableWithoutFeedback } from 'react-native'
import { globalStyle } from '../Stylesheet';
const {width } = Dimensions.get('screen')
import { useNavigation } from "@react-navigation/native";
const EmployeeList = (props) => {
    const navigation = useNavigation()
    const [enable, setEnable] = useState(true)

    const goToEmployeeInfo = (val) => {
      
      navigation.navigate('EmployeeDetails', {
        details: val
      })
    }

    const renderItem = ({item, index}) => {

        return (
          <Animated.View>
            <TouchableOpacity 
                onPress={() => goToEmployeeInfo(item)}
                style={[style.employee, globalStyle.shadow]}>
              <View style={style.avatar}>
                <FontAwesomeIcon icon={faUser} size={40} color="#933401" />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 20,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <View style={{width: '60%'}}>
                  <Text numberOfLines={1}>{item.name}</Text>
                  <Text numberOfLines={1}>{item.email}</Text>
                </View>

                <View>
                  <TouchableOpacity
                    onPress={() => props.delete(item)}
                    style={[
                      globalStyle.button,
                      {borderRadius: 5, marginTop: 10},
                      globalStyle.shadow,
                    ]}>
                    <FontAwesomeIcon icon={faTrash} color={"#fff"} />
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableOpacity>
          </Animated.View>
        );
    }
    return (
        <FlatList
            scrollEnabled={enable}
        contentContainerStyle={{alignItems:"center"}}
           data={props.data}
           extraData={props.data}
           renderItem={renderItem}     
        />
    )
}

const style  = StyleSheet.create({
    employee:{
        paddingVertical: 30,
        paddingHorizontal: 20,
        backgroundColor:"#fff",
        marginTop: 20,
        flexDirection:"row",
        alignItems:"center",
        width: width * .9,
        borderRadius: 10,
      
    },
    avatar:{
        height: 70,
        width: 70,
        borderRadius: 35,
        backgroundColor:"#c5c5c5",
        alignItems:'center',justifyContent:"center"
    }
})


export default EmployeeList
