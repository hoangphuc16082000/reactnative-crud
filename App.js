import React from 'react'
import { View, Text } from 'react-native'
import { AuthStack } from './src/navigator/Stack'

const App = () => {
  return (
    <>
    <AuthStack/>
    </>
  )
}

export default App
